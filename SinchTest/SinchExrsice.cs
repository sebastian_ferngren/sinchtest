﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SinchTest
{
    public class SinchExersice
    {
        public static bool NumberIsPowerOfTwo(int theNumber)
        { 
            return (theNumber != 0) && ((theNumber & (theNumber - 1)) == 0);
        }

        public static String ReverseString(String theString)
        {
            if (theString == null)
                throw new ArgumentNullException("String cannot be null");

            return new String(theString.ToCharArray().Reverse().ToArray());
        }

        public static String RepeatString(String theString, int times)
        {
            if (theString == null) 
                throw new ArgumentNullException("string cannot be null");
            if(times <= 0)
                throw new ArgumentOutOfRangeException("times must be bigger than 0");

            var builder = new StringBuilder();
            return builder.Insert(0, theString, times).ToString();

        }

        public static void PrintOddNumbers()
        {
            foreach (var i in Enumerable.Range(0, 100).Where(x => x % 2 == 1))
            {
                Console.WriteLine(i); 
            }
        }

    }
}
