﻿using NUnit.Framework;
using SinchTest;
using System;

namespace SinchUnitTests
{
    [TestFixture]
    public class UnitTest1
    {
        [TestFixture]
        public class NumberIsPowerOfTwo
        {
            [TestCase(0,ExpectedResult = false)]
            [TestCase(1,ExpectedResult = true)]
            [TestCase(2,ExpectedResult = true)]
            [TestCase(-2,ExpectedResult = false)]
            [TestCase(64,ExpectedResult = true)]
            public bool ReturnsTrueForNumber(int number)
            {
                return SinchExersice.NumberIsPowerOfTwo(number);
            } 
        }

        [TestFixture]
        public class ReverseString
        {
            [TestCase("abc",ExpectedResult = "cba")]
            [TestCase("123",ExpectedResult = "321")]
            [TestCase("with space",ExpectedResult = "ecaps htiw")]
            [TestCase("",ExpectedResult = "", TestName ="Empty string returns empty string")]
            [TestCase("a",ExpectedResult = "a", TestName ="Single char")]
            public String TestReverseString(String theString)
            {
                return SinchExersice.ReverseString(theString);
            }

            [Test]
            public void TestArgumentException()
            {
                Assert.Throws<ArgumentNullException>(() => SinchExersice.ReverseString(null));
            }
        }

        [TestFixture]
        public class RepeatString
        {
            [TestCase("abc",1,ExpectedResult = "abc")]
            [TestCase("abc",2,ExpectedResult = "abcabc")]
            [TestCase("space ",2,ExpectedResult = "space space ")]
            public String TestRepeatString(String theString, int times)
            {
                return SinchExersice.RepeatString(theString,times);
            }

            [Test]
            public void ThrowsArgumentErrorOnNegativeTimes()
            {
                Assert.Throws<ArgumentOutOfRangeException>(() => SinchExersice.RepeatString("will throw error",-1));
            }

            [Test]
            public void ThrowsArgumentErrorOnNullString()
            {
                Assert.Throws<ArgumentNullException>(() => SinchExersice.RepeatString(null,2));
            }

            [Test]
            public void ThrowsArgumentErrorOnZeroTimes()
            {
                Assert.Throws<ArgumentOutOfRangeException>(() => SinchExersice.RepeatString("will throw",0));
            }
        }


    }
}
