﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication1.Models;

namespace WebApplication1.Services
{
    public class BookRepository : IBookRepository
    {
        public const string CacheKey = "BookRepo";
        public BookRepository()
        {
            var ctx = HttpContext.Current;
            if (ctx != null)
            {
                if (ctx.Cache[CacheKey] == null)
                {
                    var books = new Book[]
                    {
                        new Book() { Title = "The beginning of infinity", Id = 0, Author = "David Deutsch"},
                        new Book() { Title = "How to cook with Admiral Ahkbar", Id = 2 , Author = "R2-D2"},
                        new Book() { Title = "The Three Body Problem", Id = 4, Author = "Cixin Liu"} 
                    };

                    ctx.Cache[CacheKey] = books;
                }
            }
        }

        public bool AddBook(Book theBook)
        {
            var ctx = HttpContext.Current;
            if (ctx != null)
            {
                try
                {
                    var data = AllBooks().ToList();
                    data.Add(theBook);
                    ctx.Cache[CacheKey] = data.ToArray();
                    return true;
                }
                catch (Exception e){
                    Console.WriteLine(e.Message);
                    return false;
                }
            }
            return false;
        }

        public Book[] AllBooks()
        {
            var ctx = HttpContext.Current;
            if (ctx != null)
                return ctx.Cache[CacheKey] as Book[];

            return new Book[] {
                new Book() { Title = "Lorem Ipsu", Id = -1},
            };
        }

        public Book FindBookById(int theId)
        { 
            return AllBooks().First<Book>(x => x.Id.Equals(theId));
        }

        public IEnumerable<Book> FindBooksByName(string theName)
        {
            return AllBooks().Where(x => x.Title.Equals(theName));
        } 

        public IEnumerable<Book> GetAll()
        {
            return AllBooks();
        } 

        public void LoanBook(Book theBook)
        {
            FindBookById(theBook.Id).Loaned = true;
        }

        public void ReturnBook(Book theBook)
        {
            FindBookById(theBook.Id).Loaned = false;
        }
    }
}