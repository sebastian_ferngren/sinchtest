﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication1.Models;

namespace WebApplication1.Services
{
    public class BookDbRepository : IBookRepository
    {
        BooksDbContext mDb;
        public BookDbRepository(BooksDbContext db)
        {
            mDb = db;
        }

        public bool AddBook(Book theBook)
        {
            mDb.Books.Add(theBook);
            mDb.SaveChanges();
            return true;
        }

        public Book FindBookById(int theId)
        {
            if (!mDb.Books.Any(x => x.Id.Equals(theId)))
                return null;

            return mDb.Books.Single(x => x.Id.Equals(theId));
        }

        public IEnumerable<Book> FindBooksByName(string theName)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Book> GetAll()
        {
            return mDb.Books;
        }

        public void LoanBook(Book theBook)
        {
            var book = mDb.Books.Single(x => x.Id.Equals(theBook.Id));
            book.Loaned = true;
            mDb.SaveChanges();
        }

        public void ReturnBook(Book theBook)
        {
            var book = mDb.Books.Single(x => x.Id.Equals(theBook.Id));
            book.Loaned = false;
            mDb.SaveChanges();
        }
    }
}