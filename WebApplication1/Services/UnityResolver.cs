﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Dependencies;
using WebApplication1.Controllers;

namespace WebApplication1.Services
{
    public class UnityResolver : IDependencyResolver
    {
        IUnityContainer container;
        public UnityResolver(IUnityContainer theContainer)
        {
            if (theContainer == null)
                throw new ArgumentNullException("container cannot be null");

            container = theContainer;
        }

        public IDependencyScope BeginScope()
        {
            var child = container.CreateChildContainer();
            return new UnityResolver(child);
        }

        public void Dispose()
        {
            container.Dispose();
        }

        public object GetService(Type serviceType)
        {
            try
            {
                if (serviceType == typeof(BooksController))
                    //return new BooksController(new BookDbRepository(new BooksDbContext()));
                    return new BooksController(new BookRepository());

                return container.Resolve(serviceType);
            }
            catch
            {
                return null; 
            }
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            try
            {
                return container.ResolveAll(serviceType);
            }
            catch
            {
                return new List<object>();
            }
        }
    }
}