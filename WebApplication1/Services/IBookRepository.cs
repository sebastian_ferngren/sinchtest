﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication1.Models;

namespace WebApplication1.Services
{
    public interface IBookRepository
    {
        Book FindBookById(int theId);
        IEnumerable<Book> FindBooksByName(String theName);
        bool AddBook(Book theBook);
        void LoanBook(Book theBook);
        void ReturnBook(Book theBookj);
        IEnumerable<Book> GetAll();
    }
}