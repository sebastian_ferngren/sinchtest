﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    [Table("Books")]
    public class Book
    {
        [Key]
        public int Id { get; set; }
        public String Title { get; set; }
        public String Author { get; set; }
        public String ISBN { get; set; }
        public bool Loaned { get; set; }
    }
}