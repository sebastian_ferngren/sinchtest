﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApplication1.Models;
using WebApplication1.Services;

namespace WebApplication1.Controllers
{
    public class BooksController : ApiController
    {
        IBookRepository repo;
        public BooksController(IBookRepository repository)
        {
            this.repo = repository;
        }
        private HttpResponseMessage ErrorNoBookWithId(int id)
        {
            return Request.CreateResponse(HttpStatusCode.NotFound, String.Format("No book with id {0} found", id));
        }

        [Route("api/books/{id}/loan")]
        public HttpResponseMessage LoanBookWithId(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            if (!BookExist(id))
                return ErrorNoBookWithId(id);

            var theBook = repo.FindBookById(id);
            if (theBook.Loaned)
                return Request.CreateResponse(HttpStatusCode.NotFound, "Book already loaned");

            repo.LoanBook(theBook);
            return Request.CreateResponse(HttpStatusCode.OK, "the book was loaned");
        }

        [Route("api/books/{id}/return")]
        public HttpResponseMessage ReturnBookWithId(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            if (!BookExist(id))
                return ErrorNoBookWithId(id);

            var theBook = repo.FindBookById(id);

            if (!theBook.Loaned)
                return Request.CreateResponse(HttpStatusCode.NotFound, "Book not loaned");

            repo.ReturnBook(theBook);
            return Request.CreateResponse(HttpStatusCode.OK, "The book was returned");
        }

        [HttpPost]
        [Route("api/books")]
        public HttpResponseMessage AddBook(Book book)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            repo.AddBook(book);
            return Request.CreateResponse(HttpStatusCode.OK, "The book was added");
        }

        //GET api/books/5
        public HttpResponseMessage GetBookById(int id)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            if (!BookExist(id))
                return ErrorNoBookWithId(id);

            var theBook = repo.FindBookById(id);
            return Request.CreateResponse<Book>(System.Net.HttpStatusCode.OK, theBook);
        }

        [HttpGet]
        [Route("api/books")]
        public Book[] Get()
        { 
            return repo.GetAll().ToArray();
        }

        private bool BookExist(int id)
        {
            return repo.FindBookById(id) != null;
        }
    }
}
