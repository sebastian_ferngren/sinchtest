namespace WebApplication1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddLoaned : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Books", "Loaned", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Books", "Loaned");
        }
    }
}
