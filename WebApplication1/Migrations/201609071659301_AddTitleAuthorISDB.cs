namespace WebApplication1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTitleAuthorISDB : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Books", "Title", c => c.String());
            AddColumn("dbo.Books", "Author", c => c.String());
            AddColumn("dbo.Books", "ISBN", c => c.String());
            DropColumn("dbo.Books", "Name");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Books", "Name", c => c.String());
            DropColumn("dbo.Books", "ISBN");
            DropColumn("dbo.Books", "Author");
            DropColumn("dbo.Books", "Title");
        }
    }
}
