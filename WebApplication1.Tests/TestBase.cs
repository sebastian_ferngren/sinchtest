﻿using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApplication1.Controllers;
using WebApplication1.Models;
using WebApplication1.Services;

namespace WebApplication1.Tests
{
        [TestFixture]
        public abstract class EmptyRepoTestBase
        {
            public IBookRepository service;

            [TestFixtureSetUp]
            public void TestSetup()
            {
                service = new Mock<IBookRepository>().Object;
            } 
        } 

        [TestFixture]
        public abstract class PopulatedRepoTestBase
        {
            public IBookRepository service;
            public const int INVALID_BOOK_ID = 0;
            public const int VALID_BOOK_ID = 1;
            public const int LOANED_BOOK_ID = 2;
            public const int UNLOANED_BOOK_ID = 3;
            public const String ValidBookTitle = "abc";
            public Book LoanedBook;
            public Book UnLoanedBook;
            public Mock<IBookRepository> mMock;
            public BooksController controller;

            [SetUp]
            public void TestSetup()
            {
                LoanedBook = new Book() { Id = LOANED_BOOK_ID, Author = "Me", ISBN="200", Loaned = true, Title = "A very popular unavailable book"};
                UnLoanedBook = new Book() { Id = UNLOANED_BOOK_ID, Author = "Me", ISBN="202", Loaned = false, Title = "This can be loaned"};

                service = new Mock<IBookRepository>().Object;
                mMock = new Mock<IBookRepository>();
                mMock.Setup(foo => foo.FindBookById(It.Is<int>(x => x == INVALID_BOOK_ID))).Returns((Book)null);
                mMock.Setup(foo => foo.FindBookById(It.Is<int>(x => x == VALID_BOOK_ID))).Returns(UnLoanedBook);
                mMock.Setup(foo => foo.FindBookById(It.Is<int>(x => x == LOANED_BOOK_ID))).Returns(LoanedBook);
                mMock.Setup(foo => foo.FindBookById(It.Is<int>(x => x == UNLOANED_BOOK_ID))).Returns(UnLoanedBook);

                //Mocks for loaning and returning books
                mMock.Setup(foo => foo.LoanBook(It.IsAny<Book>())).Callback((Book x) => x.Loaned = true);
                mMock.Setup(foo => foo.ReturnBook(It.IsAny<Book>())).Callback((Book x) => x.Loaned = true); 
            } 
        }

}
