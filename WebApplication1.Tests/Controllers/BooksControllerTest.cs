﻿using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Routing;
using WebApplication1.Controllers;
using WebApplication1.Models;
using WebApplication1.Services;

namespace WebApplication1.Tests.Controllers
{
    [TestFixture]
    public class BooksControllerTest 
    {
        [TestFixture]

        public class GetAll : EmptyRepoTestBase
        { 
            [Test]
            public void ShouldReturnEmptyCollectionWhenEmpty()
            {
                var controller = new BooksController(service)
                {
                    Configuration = new System.Web.Http.HttpConfiguration(),
                    Request = new HttpRequestMessage() { Method = HttpMethod.Get, RequestUri = new Uri("http://localhost:34797/api/books") }
                };
                controller.Configuration.EnsureInitialized();

                var res =  controller.Get();
                Assert.That(res.Length, Is.EqualTo(0)); 
            }

            [Test]
            public void ShouldReturnContentOfRepository()
            {
                int ExpectedLengthOfAll = 5;
                var mock = new Mock<IBookRepository>();
                mock.Setup(x => x.GetAll()).Returns(Enumerable.Repeat<Book>(new Book(), ExpectedLengthOfAll));

                var controller = new BooksController(mock.Object)
                {
                    Configuration = new System.Web.Http.HttpConfiguration(),
                    Request = new HttpRequestMessage() { Method = HttpMethod.Get, RequestUri = new Uri("http://localhost:34797/api/books") }
                };
                controller.Configuration.EnsureInitialized();

                var res =  controller.Get();
                Assert.That(res.Length, Is.EqualTo(ExpectedLengthOfAll));
            }
        }

        [TestFixture]
        public class GetBookById : PopulatedRepoTestBase
        { 
            BooksController CreateControllerForGetWithId(int id)
            {
                var theController = new BooksController(mMock.Object)
                {
                    Configuration = new System.Web.Http.HttpConfiguration(),
                    Request = new HttpRequestMessage() { Method = HttpMethod.Get, RequestUri = new Uri(String.Format("http://localhost:34797/api/books/{0}", id)) }
                };
                theController.Configuration.EnsureInitialized();
                return theController;
            } 

            [TestCase(INVALID_BOOK_ID, ExpectedResult=HttpStatusCode.NotFound,TestName ="Returns NotFound on invalid id")]
            [TestCase(VALID_BOOK_ID, ExpectedResult=HttpStatusCode.OK,TestName ="Returns OK on valid id")]
            [TestCase(null, ExpectedResult=HttpStatusCode.NotFound,TestName ="Returns NotFound on null id")]
            public HttpStatusCode ReturnsExpectedCodeOnId(int id)
            {
                controller = CreateControllerForGetWithId(id);
                var theResult = controller.GetBookById(id);
                return theResult.StatusCode;
            } 

            public void ReturnsExpectedBookWithId()
            {
                controller = CreateControllerForGetWithId(VALID_BOOK_ID);
                var validResult = controller.GetBookById(VALID_BOOK_ID);
                var bookObj = new Book();
                var res = validResult.TryGetContentValue<Book>(out bookObj);

                Assert.That((bookObj).Title, Is.EqualTo(ValidBookTitle));
            } 
        }

        [TestFixture]
        public class LoanBookWithId : PopulatedRepoTestBase
        {
            BooksController CreateControllerForLoanWithId(int id)
            {
                var theController = new BooksController(mMock.Object)
                {
                    Configuration = new System.Web.Http.HttpConfiguration(),
                    Request = new HttpRequestMessage() { Method = HttpMethod.Post, RequestUri = new Uri(String.Format("http://localhost:34797/api/books/{0}/loan", id)) }
                };
                theController.Configuration.EnsureInitialized();
                return theController;
            } 

            [TestCase(null, ExpectedResult = HttpStatusCode.NotFound, TestName ="Returns NotFound on null id")]
            [TestCase(INVALID_BOOK_ID, ExpectedResult = HttpStatusCode.NotFound, TestName ="Returns NotFound on invalid id")]
            [TestCase(VALID_BOOK_ID, ExpectedResult = HttpStatusCode.OK, TestName ="Returns OK on valid id")]
            [TestCase(LOANED_BOOK_ID, ExpectedResult = HttpStatusCode.NotFound, TestName ="Returns NotFound on already loaned")]
            public HttpStatusCode LoanBookWithIdTest(int id)
            {
                var controller = CreateControllerForLoanWithId(id);
                var code = controller.LoanBookWithId(id).StatusCode;
                return code;
            }

            [TestCase(UNLOANED_BOOK_ID, ExpectedResult = HttpStatusCode.NotFound, TestName ="Returns NotFound when loaning without returning")]
            public HttpStatusCode LoanedBookIsUpdate(int id)
            {
                var controller = CreateControllerForLoanWithId(id);
                var res = controller.LoanBookWithId(id);
                Assert.That(res.StatusCode, Is.EqualTo(HttpStatusCode.OK), "Cannot loan book");
                var newRes = controller.LoanBookWithId(id);
                return newRes.StatusCode;
            } 
        }

        [TestFixture]
        public class ReturnBookWithId : PopulatedRepoTestBase
        {
            BooksController CreateControllerForReturnWithId(int id)
            {
                var theController = new BooksController(mMock.Object)
                {
                    Configuration = new System.Web.Http.HttpConfiguration(),
                    Request = new HttpRequestMessage() { Method = HttpMethod.Post, RequestUri = new Uri(String.Format("http://localhost:34797/api/books/{0}/return", id)) }
                };
                theController.Configuration.EnsureInitialized();
                return theController;
            } 

            [TestCase(null, ExpectedResult = HttpStatusCode.NotFound, TestName ="Returns NotFound when returning book with id null")]
            [TestCase(INVALID_BOOK_ID, ExpectedResult = HttpStatusCode.NotFound, TestName ="Returns NotFound when returning book with invalid id")]
            [TestCase(UNLOANED_BOOK_ID, ExpectedResult = HttpStatusCode.NotFound, TestName ="Returns NotFound when returning not loaned book")]
            [TestCase(LOANED_BOOK_ID, ExpectedResult = HttpStatusCode.OK, TestName ="Returns OK when returning not loaned book")]
            public HttpStatusCode TestReturnBookWithId(int id)
            {
                var controller = CreateControllerForReturnWithId(id);
                var code =  controller.ReturnBookWithId(id).StatusCode;
                if (code == HttpStatusCode.OK)
                    return code;


                return code;
            } 

            [TestCase(UNLOANED_BOOK_ID, ExpectedResult = HttpStatusCode.OK, TestName ="Returns OK when returning after loaning")]
            public HttpStatusCode TestReturnAfterLoan(int id)
            {
                var controller = CreateControllerForReturnWithId(id);
                controller.LoanBookWithId(id);
                return controller.ReturnBookWithId(id).StatusCode;
            } 
        }
    }
}
